const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const userSchema = mongoose.Schema({
    username:{
        type:String,
        required:true,
        unique:true
    },
    email:{
        type:String,
        required: true,
        unique: true
    },
    password:{
        type:String,
        required:true
    },
    createdAt:{
        type:Date,
        default:Date.now()
    }
});

// Pre-save middleware
// This function runs automatically before a User document is saved to the database
userSchema.pre('save', async function(next) {
    // Check if the password field has been modified
    if (this.isModified('password')) {
        // If yes, hash the new password
        this.password = await bcrypt.hash(this.password, 8);
    }
    // Proceed to the next middleware or save the document
    next();
});


module.exports = mongoose.model('User', userSchema);