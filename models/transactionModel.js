const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    accountId: {
        // References the Account model to associate this transaction with a specific account
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        required: true
    },
    type: {
        // Defines the type of transaction
        type: String,
        enum: ['deposit',
            'withdrawal', 'transfer'], required: true
    },
    amount: {
        // The monetary value of the transaction
        type: Number,
        required: true
    },
    date: {
        // The date and time when the transaction occurred
        type: Date,
        default: Date.now
    },
    recipientAccount: {
        // Optional field for transfer-type transactions to specify the recipient account
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account'
    }
});

module.exports = mongoose.model('Transaction', transactionSchema);
