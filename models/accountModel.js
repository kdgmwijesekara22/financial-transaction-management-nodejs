const mongoose = require('mongoose');

const accountSchema = new mongoose.Schema({
    userId: {
        // Links an account to a specific user
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    balance: {
        // Stores the financial balance of the account
        type: Number,
        default: 0
    },
    createdAt: {
        // Records the date and time when the account was created
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Account', accountSchema);
