const express = require("express");
const connectDB = require("./config/dbConnection");
const dotenv = require("dotenv").config();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger');

// Middleware imports
const errorHandler = require("./middleware/errorHandler");
const validateToken = require("./middleware/validateTokenHandler");

// Route imports
const userRoutes = require("./routes/userRoutes");
const accountRoutes = require("./routes/accountRoutes");
const transactionRoutes = require("./routes/transactionRoutes");
const filterRoutes = require("./routes/filterRoutes");

const app = express();

// Database connection
connectDB();

const PORT = process.env.PORT || 5000;

// Serve Swagger docs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Middleware
app.use(express.json());

// Routes
app.use("/api/users", userRoutes);
app.use("/api/accounts", validateToken, accountRoutes);
app.use("/api/transactions", validateToken, transactionRoutes);
app.use("/api/filters", validateToken, filterRoutes);

// Error handling middleware
app.use(errorHandler);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
