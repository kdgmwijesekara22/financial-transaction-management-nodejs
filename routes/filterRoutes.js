const express = require("express");
const { filterTransactions } = require("../controllers/filterController");
const validateToken = require("../middleware/validateTokenHandler");

const router = express.Router();

/**
 * @swagger
 * /search:
 *   get:
 *     summary: Filter transactions based on criteria
 *     tags: [Transactions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: startDate
 *         schema:
 *           type: string
 *           format: date
 *         required: false
 *         description: Start date for filtering transactions
 *       - in: query
 *         name: endDate
 *         schema:
 *           type: string
 *           format: date
 *         required: false
 *         description: End date for filtering transactions
 *       - in: query
 *         name: type
 *         schema:
 *           type: string
 *         required: false
 *         description: Type of transaction (e.g., deposit, withdrawal)
 *       - in: query
 *         name: minAmount
 *         schema:
 *           type: number
 *         required: false
 *         description: Minimum amount for filtering transactions
 *       - in: query
 *         name: maxAmount
 *         schema:
 *           type: number
 *         required: false
 *         description: Maximum amount for filtering transactions
 *     responses:
 *       200:
 *         description: Successfully retrieved filtered transactions
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Transaction'
 *       401:
 *         description: Unauthorized, token invalid or not provided
 */

router.get("/search", validateToken, filterTransactions);

module.exports = router;
