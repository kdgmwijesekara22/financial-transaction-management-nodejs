const express = require("express");
const { getAccountBalance, getAccountHistory} = require("../controllers/accountController");
const validateToken = require("../middleware/validateTokenHandler");

const router = express.Router();
/**
 * @swagger
 * /balance:
 *   get:
 *     summary: Get the current user's account balance
 *     tags: [Accounts]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Account balance retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 balance:
 *                   type: number
 *                   example: 1000
 *       404:
 *         description: Account not found
 *       401:
 *         description: Unauthorized, token invalid or not provided
 */

router.get("/balance", validateToken, getAccountBalance);

/**
 * @swagger
 * /history:
 *   get:
 *     summary: Get the transaction history of the current user's account
 *     tags: [Accounts]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Transaction history retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Transaction'
 *       404:
 *         description: No transaction history found
 *       401:
 *         description: Unauthorized, token invalid or not provided
 */
router.get("/history", validateToken, getAccountHistory);

module.exports = router;
