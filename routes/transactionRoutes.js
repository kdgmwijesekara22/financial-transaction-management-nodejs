const express = require("express");
const { processDeposit, processWithdrawal, processTransfer } = require("../controllers/transactionController");
const validateToken = require("../middleware/validateTokenHandler");

const router = express.Router();

/**
 * @swagger
 * /api/transactions/deposit:
 *   post:
 *     security:
 *       - bearerAuth: []
 *     summary: Process a deposit to an account
 *     tags: [Transactions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - accountId
 *               - amount
 *             properties:
 *               accountId:
 *                 type: string
 *                 description: The ID of the account to deposit into
 *               amount:
 *                 type: number
 *                 description: The amount to deposit
 *     responses:
 *       201:
 *         description: Deposit processed successfully
 *       400:
 *         description: Bad request
 *       404:
 *         description: Account not found
 */
router.post("/deposit", validateToken, processDeposit);

/**
 * @swagger
 * /api/transactions/withdrawal:
 *   post:
 *     security:
 *       - bearerAuth: []
 *     summary: Process a withdrawal from an account
 *     tags: [Transactions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - accountId
 *               - amount
 *             properties:
 *               accountId:
 *                 type: string
 *                 description: The ID of the account to withdraw from
 *               amount:
 *                 type: number
 *                 description: The amount to withdraw
 *     responses:
 *       201:
 *         description: Withdrawal processed successfully
 *       400:
 *         description: Bad request or insufficient funds
 *       404:
 *         description: Account not found
 */
router.post("/withdrawal", validateToken, processWithdrawal);

/**
 * @swagger
 * /api/transactions/transfer:
 *   post:
 *     security:
 *       - bearerAuth: []
 *     summary: Process a transfer between two accounts
 *     tags: [Transactions]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - fromAccountId
 *               - toAccountId
 *               - amount
 *             properties:
 *               fromAccountId:
 *                 type: string
 *                 description: The ID of the account to transfer from
 *               toAccountId:
 *                 type: string
 *                 description: The ID of the account to transfer to
 *               amount:
 *                 type: number
 *                 description: The amount to transfer
 *     responses:
 *       201:
 *         description: Transfer processed successfully
 *       400:
 *         description: Bad request or insufficient funds in source account
 *       404:
 *         description: One or both accounts not found
 */
router.post("/transfer", validateToken, processTransfer);

module.exports = router;
