const http = require('http');

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1ZTE4M2FkZjZiMTFhYzQxOTk4NWY0MiIsImlhdCI6MTcwOTI3ODEyNiwiZXhwIjoxNzExODcwMTI2fQ.8JoDTTNcNhCHtR9VhocuoPdBEjkZigWYG8As3M9jdfM';
const data = JSON.stringify({
    accountId: '65e17bd196a8a42939199329',
    amount: 50
});

const options = {
    hostname: 'localhost',
    port: 5000,
    path: '/api/transactions/withdrawal',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data),
        'Authorization': `Bearer ${token}`
    }
};

const req = http.request(options, (res) => {
    console.log(`Status Code: ${res.statusCode}`);

    res.on('data', (d) => {
        process.stdout.write("Response: " + d);
    });
});

req.on('error', (error) => {
    console.error(error);
});

req.write(data);
req.end();
