const http = require('http');

const data = JSON.stringify({
    email: 'geeth@example.com',
    password: 'passwordgeeth'
});

const options = {
    hostname: 'localhost',
    port: 5000,
    path: '/api/users/login',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
    }
};

const req = http.request(options, (res) => {
    let responseBody = '';

    console.log(`Status Code: ${res.statusCode}`);

    res.on('data', (chunk) => {
        responseBody += chunk;
    });

    res.on('end', () => {
        console.log("Response Body:", responseBody);
        const response = JSON.parse(responseBody);
        if (response.token) {
            console.log("Logged in successfully. Token:", response.token);
        }
    });
});

req.on('error', (error) => {
    console.error(error);
});

req.write(data);
req.end();
