const http = require('http');

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1ZTE4M2FkZjZiMTFhYzQxOTk4NWY0MiIsImlhdCI6MTcwOTI3ODEyNiwiZXhwIjoxNzExODcwMTI2fQ.8JoDTTNcNhCHtR9VhocuoPdBEjkZigWYG8As3M9jdfM';

const options = {
    hostname: 'localhost',
    port: 5000,
    path: '/api/accounts/history', // Replace with the actual path to your getAccountHistory API
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}` // Include the token in the Authorization header
    }
};

const req = http.request(options, (res) => {
    console.log(`Status Code: ${res.statusCode}`);

    res.on('data', (d) => {
        process.stdout.write(d);
    });
});

req.on('error', (error) => {
    console.error(error);
});

req.end();
