const http = require('http');

const queryParams = new URLSearchParams({
    startDate: '2022-01-01',
    endDate: '2022-12-31',
    type: 'deposit',
    minAmount: '100',
    maxAmount: '1000'
});
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1ZTE4M2FkZjZiMTFhYzQxOTk4NWY0MiIsImlhdCI6MTcwOTI3ODEyNiwiZXhwIjoxNzExODcwMTI2fQ.8JoDTTNcNhCHtR9VhocuoPdBEjkZigWYG8As3M9jdfM';

const options = {
    hostname: 'localhost',
    port: 5000,
    path: `/api/transactions/search?${queryParams}`,
    method: 'GET',
    headers: {
        'Authorization': `Bearer ${token}`
    }
};

const req = http.request(options, (res) => {
    console.log(`Status Code: ${res.statusCode}`);

    res.on('data', (d) => {
        process.stdout.write(d);
    });
});

req.on('error', (error) => {
    console.error(error);
});

req.end();
