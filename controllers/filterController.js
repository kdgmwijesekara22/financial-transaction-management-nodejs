const asyncHandler = require("express-async-handler");
const Transaction = require("../models/transactionModel");

// Filter transactions
const filterTransactions = asyncHandler(async (req, res) => {
    const { startDate, endDate, type, minAmount, maxAmount } = req.query;
    const accountId = req.user.accountId; // Assuming accountId is stored in req.user by the validateToken middleware

    // Build a query object to handle various filtering criteria
    let query = {
        accountId,
        ...(startDate && { date: { $gte: new Date(startDate) } }),
        ...(endDate && { date: { $lte: new Date(endDate) } }),
        ...(type && { type }),
        ...(minAmount && { amount: { $gte: minAmount } }),
        ...(maxAmount && { amount: { $lte: maxAmount } }),
    };

    const transactions = await Transaction.find(query);
    res.status(200).json(transactions);
});

module.exports = {
    filterTransactions,
};
