const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const dotenv = require("dotenv");
const Account = require("../models/accountModel"); // Import the account model

dotenv.config();

// Register a new user
// Updated registerUser function with account creation
const registerUser = asyncHandler(async (req, res) => {
    const { username, email, password } = req.body;
    if (!username || !email || !password) {
        console.log("Missing fields in registration attempt", req.body);
        res.status(400);
        throw new Error("Please add all fields");
    }
    const userExists = await User.findOne({ email });
    if (userExists) {
        console.log("User already exists with email:", email);
        res.status(400);
        throw new Error("User already exists");
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    console.log("Password hashed successfully");

    try {
        const user = await User.create({
            username,
            email,
            password: hashedPassword
        });
        console.log("User created successfully:", user);

        if (user) {
            try {
                const account = await Account.create({ userId: user._id, balance: 0 });
                console.log("Account created successfully for user:", user._id);
                const token = generateToken(user._id);
                res.status(201).json({
                    _id: user._id,
                    username: user.username,
                    email: user.email,
                    accountId: account._id,
                    token
                });
            } catch (err) {
                console.error("Error creating account for user:", err);
                // Handle failure in account creation
            }
        }
    } catch (err) {
        console.error("Error creating user:", err);
        res.status(400);
        throw new Error("Invalid user data");
    }
});


// Authenticate a user and get token
const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body;

    console.log('Attempting to log in with:', email); // Log the email being used to login

    // Validate email and password
    const user = await User.findOne({ email });
    if (user) {
        console.log('User found in database:', user); // Log that the user was found
        console.log(`Stored hash for comparison: ${user.password}`); // Log the stored hashed password

        const passwordMatch = await bcrypt.compare(password, user.password);
        console.log(`Password match result: ${passwordMatch}`); // Log the result of the password comparison

        if (passwordMatch) {
            console.log('Password is correct. Generating token...'); // Log password match
            const token = generateToken(user._id);
            res.json({
                _id: user.id,
                username: user.username,
                email: user.email,
                token,
            });
        } else {
            console.log('Password is incorrect'); // Log password mismatch
            res.status(401).json({ message: "Invalid credentials" });
        }
    } else {
        console.log('No user found with that email'); // Log user not found
        res.status(401).json({ message: "Invalid credentials" });
    }
});


// Get current user
const currentUser = asyncHandler(async (req, res) => {
    res.json(req.user);
});

// Generate JWT Token
const generateToken = (id) => {
    return jwt.sign({ id }, process.env.ACCESS_TOKEN_SECERT, {
        expiresIn: "30d",
    });
};

module.exports = {
    registerUser,
    loginUser,
    currentUser,
};
