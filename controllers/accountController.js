const asyncHandler = require("express-async-handler");
const Account = require("../models/accountModel");
const Transaction = require("../models/transactionModel");

// Get account balance
const getAccountBalance = asyncHandler(async (req, res) => {
    const accountId = req.user.accountId;

    const account = await Account.findById(accountId);
    if (!account) {
        res.status(404);
        throw new Error("Account not found");
    }

    res.json({ balance: account.balance });
});

// Get account transaction history
const getAccountHistory = asyncHandler(async (req, res) => {
    const accountId = req.user.accountId; // Assuming accountId is stored in the user's request after validation

    const transactions = await Transaction.find({ accountId: accountId }).sort({ date: -1 });
    if (!transactions) {
        res.status(404);
        throw new Error("No transaction history found");
    }

    res.json(transactions);
});

module.exports = {
    getAccountBalance,
    getAccountHistory,
};
