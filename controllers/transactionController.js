const asyncHandler = require("express-async-handler");
const Transaction = require("../models/transactionModel");
const Account = require("../models/accountModel");

const processDeposit = asyncHandler(async (req, res) => {
    const { accountId, amount } = req.body;

    if (!amount || amount <= 0) {
        res.status(400);
        throw new Error("Deposit amount must be positive");
    }

    const account = await Account.findById(accountId);
    if (!account) {
        res.status(404);
        throw new Error("Account not found");
    }

    account.balance += amount;
    await account.save();

    const transaction = await Transaction.create({
        accountId,
        amount,
        type: 'deposit'
    });

    res.status(201).json(transaction);
});

const processWithdrawal = asyncHandler(async (req, res) => {
    const { accountId, amount } = req.body;

    if (!amount || amount <= 0) {
        res.status(400);
        throw new Error("Withdrawal amount must be positive");
    }

    const account = await Account.findById(accountId);
    if (!account) {
        res.status(404);
        throw new Error("Account not found");
    }

    if (account.balance < amount) {
        res.status(400);
        throw new Error("Insufficient funds");
    }

    account.balance -= amount; // Subtract the withdrawal amount
    await account.save();

    const transaction = await Transaction.create({
        accountId,
        amount,
        type: 'withdrawal'
    });

    res.status(201).json(transaction);
});

const processTransfer = asyncHandler(async (req, res) => {
    const { fromAccountId, toAccountId, amount } = req.body;

    if (!amount || amount <= 0) {
        res.status(400);
        throw new Error("Transfer amount must be positive");
    }

    const fromAccount = await Account.findById(fromAccountId);
    const toAccount = await Account.findById(toAccountId);

    if (!fromAccount || !toAccount) {
        res.status(404);
        throw new Error("One or both accounts not found");
    }

    if (fromAccount.balance < amount) {
        res.status(400);
        throw new Error("Insufficient funds in source account");
    }

    fromAccount.balance -= amount;
    toAccount.balance += amount;
    await fromAccount.save();
    await toAccount.save();

    const transactionOut = await Transaction.create({
        accountId: fromAccountId,
        amount,
        type: 'transfer out',
        recipientAccount: toAccountId // Reference to the recipient account
    });

    const transactionIn = await Transaction.create({
        accountId: toAccountId,
        amount,
        type: 'transfer in',
        recipientAccount: fromAccountId // Reference to the source account
    });

    res.status(201).json({
        transactionOut,
        transactionIn
    });
});


module.exports = {
    processDeposit,
    processWithdrawal,
    processTransfer,
};
